Copyright &copy; 2016-2017 Kirk Rader

# cppExamples

C++ implementation of a thread-synchronization class modeled on Java's "monitors."

This exists as an example both of wrapping a native API in a C++ class
and as a demonstration of basic multi-threaded application design.
