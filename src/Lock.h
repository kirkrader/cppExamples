/*
 * Copyright 2015--2016 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LOCK_H_
#define LOCK_H_

#include "Monitor.h"

/*!
 * \file Lock.h
 *
 * \brief Declarations for Lock
 */

namespace examples {

/*!
 * \class Lock
 *
 * \brief Lock on a Monitor
 */
class Lock {

public:

    /*!
     * \brief Lock the given Monitor.
     *
     * \param mtr The Monitor to lock.
     */
    explicit Lock(Monitor& mtr) :
            monitor(mtr) {

        monitor.lock();

    }

    /*!
     * \brief Unlock mutex
     */
    virtual ~Lock() {

        monitor.unlock();

    }

private:

    /*!
     * \brief Prevent copying instances of this class.
     */
    Lock(const Lock&);

    /*!
     * \brief Prevent assigning instances of this class.
     */
    Lock& operator=(const Lock&);

    /*!
     * \brief The locked pthread_mutex_t
     */
    Monitor& monitor;

};

}

#endif /* LOCK_H_ */
