/*
 * Copyright 2015-2016 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef COMMAND_H_
#define COMMAND_H_

/*!
 * \file Command.h
 *
 * \brief Declarations for examples::Command
 */

namespace examples {

/*!
 * \class Command
 *
 * \brief Objects that can be invoked asynchronously using CommandQueue
 *
 * \see CommandQueue::enqueue(Command*)
 */
class Command {

public:

    /*!
     * \brief Virtual destructor
     */
    virtual ~Command() {

        // nothing to do in the base class

    }

    /*!
     * \brief Method invoked asynchronously using CommandQueue
     *
     * \see CommandQueue::enqueue(Command*)
     */
    virtual void execute() = 0;

};

}

#endif /* COMMAND_H_ */
