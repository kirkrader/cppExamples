/*
 * Copyright 2015-2016 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "CompositeCommand.h"

#include <stdarg.h>
#include <stdexcept>

namespace examples {

CompositeCommand::CompositeCommand(size_t count, ...) {

    va_list alist;
    va_start(alist, count);

    for (size_t index = 0; index < count; ++index) {

        Command* command = va_arg(alist, Command*);

        if (0 == command) {

            throw std::domain_error("Command may not be null");

        }

        commands.push_back(command);

    }

    va_end(alist);

}

void CompositeCommand::execute() {

    std::deque<Command*>::iterator current = commands.begin();
    std::deque<Command*>::iterator end = commands.end();

    while (current != end) {

        Command* command = *current++;
        command->execute();

    }
}

}
