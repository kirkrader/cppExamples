/*
 * Copyright 2016 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Thread.h"

#include <stdexcept>

namespace examples {

void* GenericThread::run(void* p) {

    GenericThread* genericThread = reinterpret_cast<GenericThread*>(p);
    return genericThread->_execute(p);

}

GenericThread::GenericThread(void* arg) :
        argument(arg) {

    pthread_attr_t attr;
    pthread_attr_init(&attr);
    int result = pthread_create(&thread, &attr, run, this);
    pthread_attr_destroy(&attr);

    if (0 != result) {

        throw std::logic_error("error starting thread");

    }
}

GenericThread::~GenericThread() {

    pthread_detach(thread);

}

void* GenericThread::_join() {

    void* result;
    pthread_join(thread, &result);
    return result;

}

}
