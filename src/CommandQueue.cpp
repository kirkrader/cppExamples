/*
 * Copyright 2015-2016 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "CommandQueue.h"

#include <exception>
#include <iostream>

#include "Lock.h"

namespace examples {

CommandQueue::CommandQueue() :
        Thread<void, void>(0) {

}

void CommandQueue::enqueue(Command* command) {

    Lock lock(monitor);
    queue.push_back(command);
    monitor.broadcast();

}

Command* CommandQueue::dequeue() {

    Lock lock(monitor);

    while (queue.empty()) {

        monitor.wait();

    }

    Command* command = queue.front();
    queue.pop_front();
    return command;

}

void* CommandQueue::execute(void*) {

    Command* command;

    while (0 != (command = dequeue())) {

        try {

            command->execute();

        } catch (std::exception& e) {

            std::cerr << e.what() << std::endl;

        }
    }

    return 0;

}

}
