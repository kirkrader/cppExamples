/*
 * Copyright 2015-2016 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef COMPOSITECOMMAND_H_
#define COMPOSITECOMMAND_H_

#include <stddef.h>
#include <deque>

#include "Command.h"

namespace examples {

/*!
 * \class CompositeCommand
 *
 * \brief Combine a sequence of Command instances into one.
 */
class CompositeCommand: public Command {

public:

    /*!
     * \brief Initialize commands.
     *
     * The first parameter is the number of Command instances.
     * The remaining arguments must be pointers to those Command
     * instances.
     *
     * \param count The number of Command instances.
     */
    explicit CompositeCommand(size_t count, ...);

    /*!
     * \brief Execute commands.
     */
    void execute();

private:

    /*!
     * The Command instances to execute.
     *
     * \see execute()
     */
    std::deque<Command*> commands;

};

}

#endif /* COMPOSITECOMMAND_H_ */
